const express = require('express');
const router = express.Router();
const db = require('../connection');
const redis = require('redis')

let client = redis.createClient()

client.on('connect', function () {
  console.log('Connected to Redis')

})

router.get('/:id', (req, res) => {
  const id = req.params.id

  client.hgetall(id, function(err, obj) {
    if (!obj) {
      db.query("SELECT * from users_data WHERE id =?", [id], (err, data) => {
        if (err) {
          return res.status(404).send();
        }
        return res.status(200).send(data)
      })
    } else {
      return res.status(200).send(obj)
    }
  })

});

router.post('/', (req, res) => {
  const {firstname, lastname, gender } = req.body
  db.query("INSERT INTO users_data (firstname, lastname, DOB, gender) VALUES ('"+firstname+"', '"+lastname+"', NOW(), '"+gender+"')", (err, result) => {
    if (err) {
      return res.status(404).send();
    }
    client.hmset(result.insertId, [
        'id', result.insertId,
        'firstname', firstname,
        'lastname', lastname,
        'gender', gender
    ], function (err) {
      if(err) {
        console.log(err)
      }
    })
    return res.status(200).send(result)
  })
});


router.put('/', (req, res) => {
  const {firstname, lastname, gender, id} = req.body
  const sql = "UPDATE users_data set firstname =? , lastname =?, gender =?  WHERE id = ?";

  db.query(sql, [firstname, lastname, gender, id], function(err, result) {
    if (err) {
      return res.status(404).send();
    }
    return res.status(200).send(result)
  });
});

module.exports = router;
