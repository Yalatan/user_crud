const express = require('express');
const bodyParser = require('body-parser');
const userRoute = require('./routes/users');
const app = express();

const port = 5000;

// configure middleware
app.set('port', process.env.port || port); // set express to use this port
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // parse form data client

app.use('/user', userRoute);

// set the app to listen on the port
app.listen(port, () => {
  console.log(`Server running on port: ${port}`);
});
